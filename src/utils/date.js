export function formatDate(date, format) {
    if (/(y+)/.test(format))
        format = format.replace(
            RegExp.$1,
            (date.getFullYear() + "").substr(4 - RegExp.$1.length)
        );
    var o = {
        "M+": date.getMonth() + 1, //month
        "d+": date.getDate(), //day
        "h+": date.getHours(), //hour
        "m+": date.getMinutes(), //minute
        "s+": date.getSeconds(), //second
    };
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(format))
            var str = o[k] + ''
        format = format.replace(
            RegExp.$1,
            (RegExp.$1.length === 1) ? str : padLeftZero(str)
        );
    }
    return format;
}

function padLeftZero(str) {
    return ("00" + str).substr(str.length)
}
