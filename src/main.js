import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import mixins from './mixins';
import httpsURL from 'axios';
import 'css/base.less';
import VueParticles from 'vue-particles'
Vue.use(VueParticles)
//vue-cookies
import VueCookies from 'vue-cookies'
Vue.use(VueCookies)

import BaiduMap from 'vue-baidu-map'
 
Vue.use(BaiduMap, {
    ak: 'eDPHsOaBW0VsvKnXIeku3YaR7SrgMWU1'
});

//公共组件
import leftMenu from "./layout/left-menu";
import topHeader from "./layout/top-header.vue"
Vue.component("leftMenu", leftMenu)
Vue.component("topHeader",topHeader)

//element ui库
Vue.use(ElementUI);

// httpsURL.defaults.baseURL = 'http://localhost:8078/halfmoon/';
// httpsURL.defaults.baseURL = process.env.VUE_APP_API_URL;

Vue.mixin(mixins);
Vue.config.productionTip = false;
Vue.use(router)

// 引入 element-ui 样式库
import 'element-ui/lib/theme-chalk/index.css';
import 'css/element_theme.scss';

// 引入 element-ui 组件库 (按需引入)
import ElementUI from 'element-ui';
import { Button } from "element-ui";
const components = [Button];
for (let key of components) {
    Vue.component(key.name, key);
}

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');