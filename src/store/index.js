import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

import demo from './module/demo';

export default new Vuex.Store({
    strict: debug,
    state: {
        tip: '欢迎使用Vuex！',
        user: []
    },
    getters: {
        tip: state => state.tip,
        user: state => state.user
    },
    mutations: {
        setTip(state, data) {
            state.tip = data;
        },
        setUser(state, data) {
            state.user = data
        },
        deleteUser(state,data) {
            state.user = []
        }
    },
    actions: {},
    modules: {
        demo
    }
})
