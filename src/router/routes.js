function load(component) {
    return resolve => require([`../views/${component}`], resolve);
}
//layout 布局
const baseView = () => import("@/layout/base-view")
const rightView = () => import("@/layout/right-content")

export const routes = [
    {
        path: '',
        redirect: '/index',
        name: 'baseView',
        component: baseView,
        meta: {
            title: 'layout布局'
        },
        children: [
            {
                path: '/index',
                name: '首页',
                hidden: false,//是否隐藏，用来做动态
                component: load('index'),
                title: '首页',
                // requireLogin: true, //是否拦截
                icon: 'el-icon-s-home',

            },
            {
                path: '/bill',
                name: '票据管理',
                hidden: false,//是否隐藏
                requireLogin: true,
                component: rightView,
                title: '票据管理',
                icon: 'el-icon-tickets',
                children: [
                    {
                        path: '/billRelease',
                        name: '票据分发',
                        hidden: false,//是否隐藏
                        component: load('bill/billRelease'),
                        title: '票据分发',
                        icon: 'el-icon-edit',

                    }
                ]
            },
            {
                path: '/billControl',
                name: '接货管理',
                hidden: false,//是否隐藏
                requireLogin: true,
                component: rightView,
                title: '接货管理',
                icon: 'el-icon-tickets',
                children: [
                    {
                        path: '/editGoodsBill',
                        name: '填写接货单',
                        hidden: false,//是否隐藏
                        component: load('billControl/editGoodsBill'),
                        title: '填写接货单',
                        icon: 'el-icon-edit',

                    },
                    {
                        path: '/goodsBillDetail',
                        name: '查询接货单',
                        hidden: false,//是否隐藏
                        component: load('billControl/goodsBillDetail'),
                        title: '查询接货单',
                        icon: 'el-icon-edit',

                    }
                ]
            },
            {
                path: '/vehicleControl',
                name: '配车管理',
                hidden: false,//是否隐藏
                requireLogin: true,
                component: rightView,
                title: '配车管理',
                icon: 'el-icon-tickets',
                children: [
                    {
                        path: '/editCargoReceipt',
                        name: '填写运输合同',
                        hidden: false,//是否隐藏
                        component: load('vehicleControl/editCargoReceipt'),
                        title: '填写运输合同',
                        icon: 'el-icon-edit',

                    },
                    {
                        path: '/cargoReceiptDetail',
                        name: '查询运输合同',
                        hidden: false,//是否隐藏
                        component: load('vehicleControl/cargoReceiptDetail'),
                        title: '查询运输合同',
                        icon: 'el-icon-edit',

                    },
                ]
            },
            {
                path: '/getGoodsControl',
                name: '到货管理',
                hidden: false,//是否隐藏
                requireLogin: true,
                component: rightView,
                title: '到货管理',
                icon: 'el-icon-tickets',
                children: [
                    {
                        path: '/driverCallback',
                        name: '到货回执',
                        hidden: false,//是否隐藏
                        component: load('getGoodsControl/driverCallback'),
                        title: '到货回执',
                        icon: 'el-icon-edit',

                    },
                    {
                        path: '/verify',
                        name: '到货验收',
                        hidden: true,//是否隐藏
                        component: load('getGoodsControl/verify'),
                        title: '到货验收',
                        icon: 'el-icon-edit',

                    },

                ]
            },
            {
                path: '/pass',
                name: '中转管理',
                hidden: false,//是否隐藏
                requireLogin: true,
                component: rightView,
                title: '中转管理',
                icon: 'el-icon-tickets',
                children: [
                    {
                        path: '/write',
                        name: '中转公司',
                        hidden: false,//是否隐藏
                        component: load('pass/write'),
                        title: '中转公司',
                        icon: 'el-icon-edit',

                    },
                    {
                        path: '/transferInfo',
                        name: '中转信息',
                        hidden: false,//是否隐藏
                        component: load('pass/transferInfo'),
                        title: '中转信息',
                        icon: 'el-icon-edit',

                    },

                ]
            },
            {
                path: '/clearControl',
                name: '结算管理',
                hidden: false,//是否隐藏
                requireLogin: true,
                component: rightView,
                title: '结算管理',
                icon: 'el-icon-tickets',
                children: [
                    {
                        path: '/goodsBillManage',
                        name: '货运单结算',
                        hidden: false,//是否隐藏
                        component: load('clearControl/goodsBillManage'),
                        title: '货运单结算',
                        icon: 'el-icon-edit',

                    },
                    {
                        path: '/transportManage',
                        name: '运输合同结算',
                        hidden: false,//是否隐藏
                        component: load('clearControl/transportManage'),
                        title: '运输合同结算',
                        icon: 'el-icon-edit',

                    },
                    {
                        path: '/proxyFee',
                        name: '代收款结算',
                        hidden: false,//是否隐藏
                        component: load('clearControl/proxyFee'),
                        title: '代收款结算',
                        icon: 'el-icon-edit',

                    },
                    {
                        path: '/extraFee',
                        name: '其它结算',
                        hidden: false,//是否隐藏
                        component: load('clearControl/extraFee'),
                        title: '其它结算',
                        icon: 'el-icon-edit',

                    },
                ]
            },
            {
                path: '/customerService',
                name: '客户服务',
                hidden: false,//是否隐藏
                requireLogin: true,
                component: rightView,
                title: '客户服务',
                icon: 'el-icon-tickets',
                children: [
                    {
                        path: '/informGet',
                        name: '提货回告',
                        hidden: false,//是否隐藏
                        component: load('customerService/informGet'),
                        title: '提货回告',
                        icon: 'el-icon-edit',

                    },
                    {
                        path: '/informArrive',
                        name: '到货回告',
                        hidden: false,//是否隐藏
                        component: load('customerService/informArrive'),
                        title: '到货回告',
                        icon: 'el-icon-edit',

                    },
                    {
                        path: '/informPass',
                        name: '中转回告',
                        hidden: false,//是否隐藏
                        component: load('customerService/informPass'),
                        title: '中转回告',
                        icon: 'el-icon-edit',

                    },
                    {
                        path: '/informGot',
                        name: '已提回告',
                        hidden: false,//是否隐藏
                        component: load('customerService/informGot'),
                        title: '已提回告',
                        icon: 'el-icon-edit',

                    },
                ]
            },
            {
                path: '/checkControl',
                name: '成本核算',
                hidden: false,//是否隐藏
                requireLogin: true,
                component: rightView,
                title: '成本核算',
                icon: 'el-icon-tickets',
                children: [
                    {
                        path: '/extraIncome',
                        name: '收入录入',
                        hidden: false,//是否隐藏
                        component: load('checkControl/extraIncome'),
                        title: '收入录入',
                        icon: 'el-icon-edit',

                    },
                    {
                        path: '/expend',
                        name: '支出录入',
                        hidden: false,//是否隐藏
                        component: load('checkControl/expend'),
                        title: '支出录入',
                        icon: 'el-icon-edit',

                    },
                    {
                        path: '/detail',
                        name: '成本报表',
                        hidden: true,//是否隐藏
                        component: load('checkControl/detail'),
                        title: '成本报表',
                        icon: 'el-icon-edit',

                    },
                ]
            },
            {
                path: '/appControl',
                name: '应用管理',
                hidden: false,//是否隐藏
                requireLogin: true,
                component: rightView,
                title: '应用管理',
                icon: 'el-icon-tickets',
                children: [
                    {
                        path: '/customerManage',
                        name: '客户管理',
                        hidden: false,//是否隐藏
                        component: load('appControl/customerManage'),
                        title: '客户管理',
                        icon: 'el-icon-edit',

                    },
                    {
                        path: '/driverManage',
                        name: '司机管理',
                        hidden: false,//是否隐藏
                        component: load('appControl/driverManage'),
                        title: '司机管理',
                        icon: 'el-icon-edit',

                    },
                    {
                        path: '/routeManage',
                        name: '线路管理',
                        hidden: false,//是否隐藏
                        component: load('appControl/routeManage'),
                        title: '线路管理',
                        icon: 'el-icon-edit',

                    },
                    {
                        path: '/employeeManage',
                        name: '职员管理',
                        hidden: false,//是否隐藏
                        component: load('appControl/employeeManage'),
                        title: '职员管理',
                        icon: 'el-icon-edit',

                    },
                ]
            },
            {
                path: '/systemControl',
                name: '系统管理',
                hidden: false,//是否隐藏
                requireLogin: true,
                component: rightView,
                title: '系统管理',
                icon: 'el-icon-tickets',
                children: [
                    {
                        path: '/userGroupManage',
                        name: '用户组设置',
                        hidden: false,//是否隐藏
                        component: load('systemControl/userGroupManage'),
                        title: '用户组设置',
                        icon: 'el-icon-edit',
                    },
                    {
                        path: '/changePassword',
                        name: '修改密码',
                        hidden: false,//是否隐藏
                        component: load('systemControl/changePassword'),
                        title: '修改密码',
                        icon: 'el-icon-edit',
                    }
                ]
            },


        ]
    },
    ,
    {
        path: '/login',
        name: 'login',
        title: '登陆页面',
        icon: 'el-icon-arrow-right',
        component: load('login/index'),
        // requireLogin: false,
    }

];

export default routes;