import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';
import stroe from "@/store/index"
import { checkToken } from "@/api/axios";
import VueCookies from 'vue-cookies'
Vue.use(VueCookies)

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'hash',
    base: process.env.BASE_URL,
    routes,
    scrollBehavior(to, from, savePosition) {
        if (savePosition) {
            return savePosition;
        } else {
            return { x: 0, y: 0 };
        }
    }
});
//前置拦截
router.beforeEach((to, from, next) => {
    var user = VueCookies.get("loginId")
    if (user !== null) {
        stroe.commit("setUser", user);
        next()
    } else {
        if (to.path === '/login') {
            console.log("进来了")
            next()
        } else {
            if (stroe.getters.user.length === 0) {
                next({
                    path: '/login'
                })
            } else {
                next();
            }
        }
    }



});
//后置拦截
router.afterEach((to, from) => {
    // console.log("afterEach-to==>", to)
    // console.log("afterEach-from==>", from)
    window.scrollTo(0, 0);
});

export default router;