import http from '@/api/http'

// 获取所有未发过 {提货 | 到货 | 中转 | 已提 | 代收} 回告的运单
export const findOnWayBills = () => {
    return http.requestGet("mam/transfer/findOnWayBills")
}

// 获取所有已发过 {提货 | 到货 | 中转 | 已提 | 代收} 回告的运单
export const findOldInform = (type, page, size) => {
    return http.requestGet("mam/goodsBill/findOldInform?type=" + type + "&pageNo=" + page + "&pageSize=" + size)
}

// 新单回告
export const addInfo = (callbackInfo) => {
    return http.requestPost("mam/callback/addInfo", callbackInfo)
}

// 查询一条回告信息
export const findDetail = (goodsBillCode) => {
    return http.requestGet("mam/goodsBill/selectByCode?goodsBillCode=" + goodsBillCode)
}
export const findDetail1 = (goodsBillCode,type) => {
    // return http.requestGet("mam/goodsBill/selectByCode?goodsBillCode=" + goodsBillCode)
    return http.requestGet("mam/callback/findDetail?goodsBillId=" + goodsBillCode+"&type="+type)
}