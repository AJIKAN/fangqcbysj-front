import http from '@/api/http'

//获取中转历史
export const arriveGoods = (driverId) => {
    return http.requestGet("/mam/goodsBill/arriveGoods?driverId=" + driverId)
}
//查找司机
export const selectById = (id) => {
    return http.requestGet("/mam/driverInfo/selectById?id=" + id)
}

//查询货运回执单号
export const findRevertId = (goodsBillCode) => {
    return http.requestGet("/mam/cargoReceipt/findRevertId?goodsBillCode=" + goodsBillCode)
}
//到货
export const addArrived = (data) => {
    return http.requestPost("/mam/bill/addArrived", data)
}

//到货 kehu
export const addCusRec = (data) => {
    return http.requestPost("/mam/customer/addCusRec", data)
}
//获取一个用户的代收货物
export const findWait = (customerCode) => {
    return http.requestGet("/mam/goodsBill/findWait?customerCode=" + customerCode)
}
//查询id单个货运单
export const selectByCode = (goodsBillCode) => {
    return http.requestGet("/mam/goodsBill/selectByCode?goodsBillCode=" + goodsBillCode)

}