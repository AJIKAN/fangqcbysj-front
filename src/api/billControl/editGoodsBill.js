import http from '@/api/http'

//填写接货单，查询客户
export const selectAllCusCode = () => {
    return http.requestGet("/mam/customer/selectAllCusCode")
}
//查询用户详情
export const selectCusByCode = (customerCode) => {
    return http.requestGet("/mam/customer/selectCusByCode?customerCode=" + customerCode)
}
//添加货运单
export const addGoodsBill = (data) => {
    return http.requestPost("/mam/goodsBill/addGoodsBill", data)
}
//添加货物
export const addGoods = (goodsBillDetailId, data) => {
    return http.requestPost("/mam/goodsBill/addGoods?goodsBillDetailId=" + goodsBillDetailId, data)
}
