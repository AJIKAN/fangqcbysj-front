import http from '@/api/http'

//获取所有订单  
export const selectAllGoodsBills = (page, size) => {
    return http.requestGet("/mam/goodsBill/selectByEvent?pageNo=" + page + "&pageSize=" + size)
}
//通过订单号查询详情
export const selectByCode = (data) => {
    return http.requestGet("/mam/goodsBill/selectByCode?goodsBillCode="+data)
}

//通过订单号更新
export const updateGoodsBill = (goodsBill,data) => {
    return http.requestPut("/mam/goodsBill/updateByCode?goodsBill="+goodsBill,data)
}
export const deleteGoodsBill = (goodsBill) => {
    return http.requestPost("/mam/goodsBill/deleteByCode?goodsBillCode="+goodsBill)
}