import http from '@/api/http'


//查询所有
export const selectAll = (page, size) => {
    return http.requestGet("/mam/bill/findByPage?pageNo=" + page + "&pageSize=" + size)
}

//查询所有领票人
export const selectAllId = () => {
    return http.requestGet("/mam/driverInfo/selectAllId")
}


//查询所有未分发
export const selectNoAll = (page, size) => {
    return http.requestGet("/mam/bill/findNotRelease?pageNo=" + page + "&pageSize=" + size)
}

//分发
export const addRelease = (billCode,data) => {
    return http.requestPost("/mam/bill/addRelease?billCode="+billCode, data)
}
// 导出列表
export const exportExcel = (page, size) => {
    return http.requestGet("/mam/bill/findByPage/export?pageNo=" + page + "&pageSize=" + size)
}
