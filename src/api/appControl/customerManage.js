import http from '@/api/http'

// 客户查询
export const selectAllCus = (page, size) => {
    return http.requestGet("mam/customer/selectAllCus?pageNo=" + page + "&pageSize=" + size)
}
// 查询客户详情
export const selectCusByCode = (customerCode) => {
    return http.requestGet("mam/customer/selectCusByCode?customerCode=" + customerCode)
}
// 顾客信息更新
export const updateCustomerInfo = (customerCode, customerInfo) => {
    return http.requestPut("mam/customer/updateCustomerInfo?customerCode=" + customerCode, customerInfo)
}
// 添加顾客
export const addCus = (customer) => {
    return http.requestPost("mam/customer/addCus", customer)
}
// 刪除客户
export const deleteCus = (customerCode) => {
    return http.requestDelete("mam/customer/deleteCus?customerCode=" + customerCode)
}
// 分页查询司机
export const selectAllByPage = (page, size) => {
    return http.requestGet("mam/driverInfo/selectAllByPage?pageNo=" + page + "&pageSize=" + size)
}
// 查询一个司机信息
export const selectById = (id) => {
    return http.requestGet("mam/driverInfo/selectById?id=" + id)
}
// 修改一个司机信息
export const update = (id, driverInfo) => {
    return http.requestPut("mam/driverInfo/update?id=" + id, driverInfo)
}
// 删除一个司机信息
export const deleteDriver = (id) => {
    return http.requestDelete("mam/driverInfo/deleteDriver?id=" + id)
}
// 新增司机信息
export const addDriver = (driverInfo) => {
    return http.requestPost("mam/driverInfo/addDriver", driverInfo)
}
// 得到所有的城市范围信息
export const findAllExpands = (page, size) => {
    return http.requestGet("mam/route/findAllExpands?pageNo=" + page + "&pageSize=" + size)
}
// 得到所有城市
export const findAllRegions = () => {
    return http.requestGet("mam/route/findAllRegions")
}
// 更新一条城市信息
export const updateExpand = (cityExpand) => {
    return http.requestPut("mam/route/updateExpand", cityExpand)
}
// 删除一条城市扩充信息
export const deleteExpand = (id) => {
    return http.requestDelete("mam/route/deleteExpand?id=" + id)
}
// 得到一条城市扩充信息
export const findExpand = (id) => {
    return http.requestGet("mam/route/findExpand?id=" + id)
}
// 得到无范围的城市
export const findLeftRegions = () => {
    return http.requestGet("mam/route/findLeftRegions")
}
// 新增城市扩充信息
export const addExpand = (cityExpand) => {
    return http.requestPost("mam/route/addExpand", cityExpand)
}
// 得到所有线路信息
export const findAllRoutes = () => {
    return http.requestGet("mam/route/findAllRoutes")
}
// 查询员工
export const selectAllEmp = (page, size) => {
    return http.requestGet("mam/employee/selectAllEmp?pageNo=" + page + "&pageSize=" + size)
}
// 查询员工
export const selectEmpByCode = (employeeCode) => {
    return http.requestGet("mam/employee/selectEmpByCode?employeeCode=" + employeeCode)
}
// 更新员工 
export const updateEmp = (employeeDto) => {
    return http.requestPut("mam/employee/updateEmp", employeeDto)
}
// 删除员工
export const deleteEmp = (employeeCode) => {
    return http.requestDelete("mam/employee/deleteEmp?employeeCode=" + employeeCode)
}
// 查询所有用户组供添加职工使用
export const selectAllUserGroup = () => {
    return http.requestGet("mam/group/selectAllUserGroup")
}
// 添加职员
export const addEmp = (employeeDto) => {
    return http.requestPost("mam/employee/addEmp", employeeDto)
}