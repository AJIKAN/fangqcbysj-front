import http from '@/api/http'
// 查询用户组
export const selectAllGroup = (page, size) => {
    return http.requestGet("mam/group/selectAllGroup?pageNo=" + page + "&pageSize=" + size)
}

// 查询用户组
export const selectFunc = (loginId) => {
    return http.requestGet("mam/group/selectFunc?loginId=" + loginId)
}

// 查询用户组详情
export const selectGroup = (id) => {
    return http.requestGet("mam/group/selectGroup?id=" + id)
}
// 用户组更新
export const updateDescription = (id, description) => {
    return http.requestPut("mam/group/updateDescription?id=" + id + "&description=" + description)
}
// 删除用户组
export const deleteGroup = (id) => {
    return http.requestDelete("mam/group/deleteGroup?id=" + id)
}
// 查询所有用户组供添加职工使用
export const selectAllUserGroup = () => {
    return http.requestGet("mam/group/selectAllUserGroup")
}
// 查询所有功能
export const selectAllFunction = () => {
    return http.requestGet("mam/group/selectAllFunction")
}

// 查询所选组功能
export const selectFunctionByGroup = (groupId) => {
    return http.requestGet("mam/group/selectFunctionByGroup?groupId=" + groupId)
}
// 给用户组授权
export const addNewFunc = (functionWithGroupDto) => {
    return http.requestPost("mam/group/addNewFunc", functionWithGroupDto)
}
// 添加用户组
export const addGroup = (userGroup) => {
    return http.requestPost("mam/group/addGroup", userGroup)
}
// 更改密码
export const change = (loginId, oldPassword, newPassword) => {
    return http.requestPut("mam/user/change?loginId=" + loginId + "&oldPassword=" + oldPassword + "&newPassword=" + newPassword)
}
// 登录
export const login = (loginId, password) => {
    return http.requestPost("mam/user/login?loginId=" + loginId + "&password=" + password)
}