import http from '@/api/http'

//代收结算-返回未结的所有实体(实体中能填的属性都填好)
export const selectClearHelp = (eventName) => {
    return http.requestGet("/mam/clear/selectClearHelp?eventName=" + eventName)
}
//代收结算-通过订单编号查询单个实体的已填所有信息
export const selectHelpBillClearByCode = (goodsBillCode) => {
    return http.requestGet("/mam/clear/selectHelpBillClearByCode?goodsBillCode=" + goodsBillCode)
}

//添加代收结算 
export const addCHelpClear = (data) => {
    return http.requestPut("/mam/clear/addCHelpClear", data)
}