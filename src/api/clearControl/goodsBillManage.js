import http from '@/api/http'

//获取已结订单未结订单
export const selectClearCus = (data) => {
    return http.requestGet("/mam/clear/selectclearCus?eventName=" + data)
}
//通过id查询单个货运单
export const selectByCode = (goodsBillCode) => {
    return http.requestGet("/mam/clear/selectCustomerBillClearByCode?goodsBillCode=" + goodsBillCode)
}
//客户结算（前台返回一个完整的实体）
export const addCusClear = (data) => {
    return http.requestPut("/mam/clear/addCusClear", data)
}

