import http from '@/api/http'

//司机结算-返回未结的所有实体(实体中能填的属性都填好)
export const selectClearDriver = (eventName) => {
    return http.requestGet("/mam/clear/selectClearDriver?eventName=" + eventName)
}
//通过回执单查看详情
export const selectDriverClearByCode = (goodsBillCode) => {
    return http.requestGet("/mam/clear/selectDriverClearByCode?goodsBillCode=" + goodsBillCode)
}
//司机结算
export const addDriClear = (data) => {
    return http.requestPut("/mam/clear/addDriClear", data)
}