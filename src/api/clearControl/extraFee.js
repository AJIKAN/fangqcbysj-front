import http from '@/api/http'

//代收结算-返回未结的所有实体(实体中能填的属性都填好)
export const selectAllExtraClear = (page, size) => {
    return http.requestGet("/mam/clear/selectAllExtraClear?pageNo=" + page + "&pageSize=" + size)
}
//添加杂费结算
export const addExtraClear = (data) => {
    return http.requestPost("/mam/clear/addExtraClear", data)
}