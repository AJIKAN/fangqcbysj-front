import http from './http'
import { Message } from 'element-ui';

export const success_code = 10000;

//失败消息提示
export const toast_err = (msg) => {
    Message.error(msg)
}
//成功消息提示
export const toast_succ = (msg) => {
    Message.success(msg)
}
//警告消息提示
export const toast_warning = (msg) => {
    Message.warning(msg)
}
//查询所有
export const dologin = (userId,password) => {
    return http.requestPost("/mam/user/login?loginId="+userId+"&password="+password)
}

// 客户查询
export const replace = (loginId) => {
    return http.requestPost("mam/user/replace?loginId=" + loginId)
}