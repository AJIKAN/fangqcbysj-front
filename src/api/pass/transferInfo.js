import http from '@/api/http'

//获取中转历史
export const findInfoByPage = (page, size) => {
    return http.requestPost("/mam/transfer/findInfoByPage?pageNo=" + page + "&pageSize=" + size)
}
//获取司机的中转运单
export const transferGoods = (driverId) => {
    return http.requestGet("/mam/goodsBill/transferGoods?driverId=" + driverId);
}
//查询运单中中转详情
export const detail = (goodsBillCode) => {
    return http.requestGet("/mam/transfer/detail?goodsBillCode=" + goodsBillCode);
}
//确认中转
export const addInfo = (data) => {
    return http.requestPost("/mam/transfer/addInfo", data);
}