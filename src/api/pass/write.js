import http from '@/api/http'

//查询查询中转公司信息-分页
export const findByPage = (page, size) => {
    return http.requestGet("/mam/transfer/findByPage?pageNo=" + page + "&pageSize=" + size)
}

//添加中转信息
export const addInfo = (data) => {
    return http.requestPost("/mam/transfer/addTransferCompany", data)
}

//获取城市信息
//获取地点
export const findAllRegions = () => {
    return http.requestGet("/mam/route/findAllRegions")
}