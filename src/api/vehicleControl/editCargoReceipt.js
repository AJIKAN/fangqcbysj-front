import http from '@/api/http'


//查询未被安排的货运单
export const selectLeftCodes = () => {
    return http.requestGet("/mam/cargoReceipt/selectLeftCodes")
}
//获取该货运单的人的信息
export const selectGoodsBill = (data) => {
    return http.requestGet("/mam/cargoReceipt/findGoodsBill?goodsRevertBillCode=" + data)
}
//获取地点
export const findAllRegions = () => {
    return http.requestGet("/mam/route/findAllRegions")
}
//提交运输合同
export const addCargo = (data) => {
    return http.requestPost("/mam/cargoReceipt/add", data)
}