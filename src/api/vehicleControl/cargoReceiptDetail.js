import http from '@/api/http'

//查询所有运输合同
export const selectAll = (page, size) => {
    return http.requestGet("/mam/cargoReceipt/selectAll?pageNo=" + page + "&pageSize=" + size)
}
//通过ID查询单个货运单
export const selectByCode = (goodsRevertBillCode) => {
    return http.requestGet("/mam/cargoReceipt/selectByCode?goodsRevertBillCode=" + goodsRevertBillCode)
}

//提交修改
export const update = (data) => {
    return http.requestPut("/mam/cargoReceipt/update" , data)
}
//提交修改
export const submit = (data) => {
    return http.requestPut("/mam/cargoReceipt/submit" , data)
}

//删除
export const deleteGoods = (goodsRevertBillCode) => {
    return http.requestDelete("/mam/cargoReceipt/delete?goodsRevertBillCode=" + goodsRevertBillCode)
}
