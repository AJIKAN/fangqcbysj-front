import http from '@/api/http'

// 查询所有营业外收入
export const selectExtraIncome = (page,size) => {
    return http.requestGet("mam/check/selectExtraIncome?pageNo="+page+"&pageSize="+size)
}
// 录入营业外收入
export const addExtraIncome = (extraIncome) => {
    return http.requestPost("mam/check/addExtraIncome",extraIncome)
}
// 查询员工工资
export const selectAllEmp = (page,size) => {
    return http.requestGet("mam/employee/selectAllEmp?pageNo="+page+"&pageSize="+size)
}
// 根据员工编号查询员工工资
export const findWage = (employeeCode) => {
    return http.requestGet("mam/check/findWage?employeeCode="+employeeCode)
}
// 录入员工工资
export const addWage = (wage) => {
    return http.requestPost("mam/check/addWage",wage)
}
// 查询所有财务费用
export const selectFinanceFee = (page,size) => {
    return http.requestGet("mam/check/selectFinanceFee?pageNo="+page+"&pageSize="+size)
}
// 录入财务费用
export const addFinanceFee = (financeFee) => {
    return http.requestPost("mam/check/addFinanceFee",financeFee)
}
// 查询所有管理费用
export const selectManageFee = (page,size) => {
    return http.requestGet("mam/check/selectManageFee?pageNo="+page+"&pageSize="+size)
}
// 录入管理费用
export const addManageFee = (manageFee) => {
    return http.requestPost("mam/check/addManageFee",manageFee)
}
// 根据id查询管理费用
export const findManageFee = (id) => {
    return http.requestGet("mam/check/findManageFee?id="+id)
}
// 查询当前月报
export const selectIncomeMonthly = () => {
    return http.requestGet("mam/check/selectIncomeMonthly")
}