import http from '@/api/http'

// 查询所有运输合同
export const selectAll = (page,size) => {
    return http.requestGet("mam/cargoReceipt/selectAll?pageNo="+page+"&pageSize="+size)
}

// 根据状态查询运输合同
export const selectByEvent = (backBillState,page,size) => {
    return http.requestGet("mam/cargoReceipt/backBillState?backBillState="+backBillState+"&pageNo="+page+"&pageSize="+size)
}

// 查询所有城市
export const findAllRegions = () => {
    return http.requestGet("mam/route/findAllRegions")
} 

// 查询未被安排的货运单
export const selectLeftCodes = () => {
    return http.requestGet("mam/cargoReceipt/selectLeftCodes")
} 

// 查询运输合同详情
export const selectByCode = (goodsRevertBillCode) => {
    return http.requestGet("mam/cargoReceipt/selectByCode?goodsRevertBillCode="+goodsRevertBillCode)
}

// 修改运输合同
export const update = (cargoReceipt) => {
    return http.requestPut("mam/cargoReceipt/update",cargoReceipt)
}

// 删除运输合同
export const deleteCargoReceipt = (goodsRevertBillCode) => {
    return http.requestDelete("mam/cargoReceipt/delete?goodsRevertBillCode="+goodsRevertBillCode)
}

// 运输合同发货
export const submit = (cargoReceipt) => {
    return http.requestPost("mam/cargoReceipt/submit",cargoReceipt)
}