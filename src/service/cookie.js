import Cookies from 'js-cookie';

const TokenKey = 'token';

// 获取
export function getToken() {
    return Cookies.get(TokenKey);
}

// 添加
export function setToken(token, timestamp) {
    return Cookies.set(TokenKey, token, {
        path: '/',
        export: time
    });
}

// 删除
export function removeToken() {
    return Cookies.remove(TokenKey);
}