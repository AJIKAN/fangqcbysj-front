// 封装 element-ui 消息提示、弹框提示 和 通知。
import { Message, MessageBox, Notification } from "element-ui";

/**
 * 消息提示
 * @param type { String } [ 消息提示：1.info、2.success、3.warning、4.error ]
 * @param content { String } [ 提示的消息文字 ]
 */
const toast = (type, content) => {
    Message({
        message: content,
        type: type,
        duration: 2000
    });
};

/**
 * 弹框提示
 * @param content { String } [ 弹框消息内容 ]
 * @param title { String } [ 弹框标题 ]
 */
const alert = (content, title="温馨提示！") => {
    MessageBox.alert(
        content,
        title,
        {
            showClose: false,
            customClass: "ajax-error"
        }
    );
};

/**
 * 通知提示
 * @param title { String } [ 通知标题 ]
 * @param content { String } [ 通知消息内容 ]
 * @param type { String } [ 通知类型: 1.info、2.success、3.warning、4.error ]
 * @param position { String } [ 自定义弹出位置: 1.op-right、2.top-left、3.bottom-right、4.bottom-left ]
 */
const notice = (title, content, type="info", position="top-right") => {
    switch (type) {
        case "info":
        case "success":
        case "warning":
        case "error":
            break;
        default:
            position = type;
            type = "info";
            break;
    }
    switch (position) {
        case "top-right":
        case "top-left":
        case "bottom-right":
        case "bottom-left":
            break;
        default:
            position = "top-right";
            break;
    }
    Notification({
        title: title,
        message: content,
        type: type,
        position: position
    });
};

export { toast, alert, notice };