const path = require('path');

function resolve(dir) {
    return path.resolve(__dirname, dir);
}

module.exports = {
    publicPath: "./",
    outputDir: process.env.outputDir,
    assetsDir: 'assets',
    lintOnSave: process.env.NODE_ENV === 'development',
    // 关闭source map。1、减少打包编译的时间。2、避免在生产环境中用F12开发者工具在Sources中看到源码。
    productionSourceMap: false,

    configureWebpack: {
        resolve: {
            extensions: ['.js', '.vue', '.json'],
            // 配置别名
            alias: {
                '@': resolve('src'),
                'api': resolve('src/api'),
                'assets': resolve('src/assets'),
                'css': resolve('src/assets/css'),
                'images': resolve('src/assets/images'),
                'components': resolve('src/components'),
                'mixins': resolve('src/mixins'),
                'service': resolve('src/service'),
                'store': resolve('src/store'),
                'view': resolve('src/views')
            }
        }
    },

    devServer: {
        overlay: { // 让浏览器 overlay 同时显示警告和错误
            warnings: true,
            errors: true
        },
        host: "localhost",
        port: 8080, // 端口号
        https: false, // https:{type:Boolean}
        open: false, //配置后自动启动浏览器
        hotOnly: true, // 热更新
        //proxy: 'http://localhost:8078/halfmoon/'   // 配置跨域处理,只有一个代理
        proxy: { //配置多个代理
            "/mam": {
                target: "http://localhost:8012",
                changeOrigin: true,
                ws: true,//websocket支持
                secure: false,
                pathRewrite: {
                    "^/mam": "/"
                    //        '^/api': '/api'   // 这种接口配置出来     http://XX.XX.XX.XX:8083/api/login
                    //'^/api': '/' 这种接口配置出来     http://XX.XX.XX.XX:8083/login
                }
            },
        }
    }
};